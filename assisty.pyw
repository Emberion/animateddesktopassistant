import tkinter,time,random,ctypes,asyncio,threading,numpy,pystray;
from pystray import MenuItem as item;
from PIL import Image, ImageTk;



class GeometryManager(object):
    def getScreenSize(self):
        user = ctypes.windll.user32;
        #get the screensize through ctypes
        screensize = user.GetSystemMetrics(0),user.GetSystemMetrics(1);
        return screensize;


class AsyncController(object):
    """
    def create_tasks(self,taskmethods):
        self.loop = asyncio.get_event_loop();
        tasks = [];
        tasknumber = 0;
        for i in taskmethods:
            tasks.append(self.loop.create_task(i()))
        print(tasks);
        threading.Thread(self.loop.run_until_complete(asyncio.wait(tasks)),daemon=True).start();
    """

    def create_tasks(self,taskmethods,args=()):
        #for each task in taskmethods array
        for task in taskmethods:
            #create a new thread and start it
            threading.Thread(target=task,args=args,daemon=True).start();



class ImageController(object):
    def __init__(self,bgColorToTransparent):
        #supercharge the class so that the constructor can be used even if this class is a parent class
        super(ImageController,self).__init__();
        #set the class property to the given parameter argument
        self.bgColorToTransparent = bgColorToTransparent;
        
    #load the current sprite image so it can be drawn on the canvas @takes string @void
    def loadImage(self,imageName,frameIndex):
        self.currentSpriteImage = tkinter.PhotoImage(file=("sprite/%s" % imageName), format=("gif - %i" % frameIndex));
        return self.currentSpriteImage;

    #get the current sprite image width and height @void
    def getImageGeometry(self):
        width,height = self.currentSpriteImage.width(), self.currentSpriteImage.height();
        return width,height;

    #create the canvas on which the images can be drawn. @takes tuple/list(width and height pixel values inside) @void
    def createImageBackground2(self,imageGeometry):
        width,height = imageGeometry[0],imageGeometry[1];
        #creates the canvas, sets the bg color to the color we want the mainwindow to transform into transparent. set the canvas width and height to that of the image.
        self.backgroundCanvas = tkinter.Canvas(self.mainWindow,bg=self.bgColorToTransparent,width=width,height=height);
        self.backgroundCanvas.pack();

    #create the canvas on which the images can be drawn. @takes tuple/list(width and height pixel values inside) @void
    def createImageBackground(self,imageGeometry):
        width,height = imageGeometry[0],imageGeometry[1];
        #creates the canvas, sets the bg color to the color we want the mainwindow to transform into transparent. set the canvas width and height to that of the image.
        self.backgroundCanvas = tkinter.Label(self.mainWindow,bg=self.bgColorToTransparent,width=width,height=height);
        self.backgroundCanvas.pack();

    #load the selected images and draw it on the background canvas @takes string @void
    def createImage(self,imageName,frame):
        self.loadImage(imageName,frame);
        #self.imageOnCanvas = self.backgroundCanvas.create_image(0,0,image=self.currentSpriteImage, anchor=tkinter.NW);
        self.backgroundCanvas.configure(image=self.currentSpriteImage);
        self.backgroundCanvas.image = self.currentSpriteImage;

    #method that changes the animation set for a specific character from for example idle to walking etc. @tkes strings @returns list
    def changeAnimationSet(self,charName,setName):
        #each character and their animations
        animations = {"ghost":
             {"idle":[self.loadImage("idleGhost.gif",i) for i in range(10)],
              "right":[self.loadImage("leftGhost.gif",i) for i in range(8)],
              "left":[self.loadImage("rightGhost.gif",i) for i in range(8)]}};
        #if char ghost
        if(charName.lower() == "ghost"):
            #if set idle
            if(setName.lower() == "idle"):
                #return ghost idle animation image set
                return animations["ghost"]["idle"];
            #if set walk
            elif setName.lower() == "right":
                #return ghost walk animation image set
                return animations["ghost"]["right"];
            elif setName.lower() == "left":
                return animations["ghost"]["left"];

    #method that sets the current active animation image set. @takes strings @void
    def chooseAnimationSet(self,charName="ghost",setName="idle"):
        self.currentAnimationSet = self.changeAnimationSet(charName,setName);

    #method that simulates the gif animation. @takes int @void
    def Animate(self,currentFrame):
        #grab the amount of frames from the current animation image set
        currentFrameLength = len(self.currentAnimationSet);
        currentFrame += 1;
        #if the current frame is the same as the length of the animation set then reset currentFrame to 0 to restart the animation set.
        if currentFrame == currentFrameLength:
            currentFrame = 0;
        try:
            #set the label image to the next gif frame
            self.backgroundCanvas.configure(image=self.currentAnimationSet[currentFrame]);
        #catch indexerror for if the animation transitions mid animation and the current frame has a higher index value than the length of the current animation image set
        except IndexError as e:
            self.backgroundCanvas.configure(image=self.currentAnimationSet[0]);
            currentFrame = 1;
        #recall this method with the new currentFrame index
        self.mainWindow.after(100, self.Animate,currentFrame);
        

        
        
        

    #this only has to be done once
    def executeCanvasCreation(imageName):
        self.loadImage("sprite/%s" % imageName,frame=0);
        self.createImageBackground(self.getImageGeometry());
        #return true to let the program know that the background has been created.
        return True;
        
        
        

class ScreenManager(ImageController):
    def __init__(self,bgColorToTransparent):
        #supercharge the class so its accessible to its child classes. pass along the bgcolortotransparent param.
        super(ScreenManager,self).__init__(bgColorToTransparent);

    #get the mainwindow y and x coordinates. @void
    def setCurrentCoords(self):
        self.currentX = self.mainWindow.winfo_x();
        self.currentY = self.mainWindow.winfo_y();

    #method creates and returns a numpy array of which each row represents the screen width for that specific pixel height line. the number of rows is the screen height in pixels. @returns numpy array
    def simulateScreenPixels(self):
        screenArray = numpy.array([[i for i in range(self.screenWidth + 1)] for i in range(self.screenHeight)]);
        return screenArray;

    #method that returns a random set of x,y coordinates for the assistant to walk to. @returns tuple
    def chooseWindowPosition(self):
        #simulate the screen pixels as 2d numpy arrays with the rows containing the y axis pixels and the contents of each row the x axis of that specific row. 
        screenPixels = self.simulateScreenPixels();
        #grab the x axis ranges from 400 to -400 index
        xRanges = screenPixels[0][400:-400];
        #grab the y axis ranges from 400 to -400 index
        yRanges = screenPixels[400:-400];
        #choose a random valid index(row) from the numpy arrays containing all the x coordinate. Each row is a pixel of the y axis. the chosen index is the chosen y coordinate for the y axis. 
        yRangeChoice = random.randint(0,len(yRanges) - 1);
        #create the tuple containing the random chosen x,y target coordinates.
        currentPositionChoice = tuple((random.choice(xRanges),yRangeChoice));
        #return the chosen coordinates
        return currentPositionChoice;

    #Method that gets the difference in numbers between two numbers. Also tells the program if the difference is negative or positive(true,false) This way the program will have data to decide if it should walk to the left/up or right/bottom in the getAllPathPixels method. @@takes int, int @returns list
    def getDifference(self,x,y):
        if x > self.currentX:
            #get the amount of X axis pixels the assistant has to walk across the screen
            walkPathX = x - self.currentX;
            #true for walking towards right on the x axis
            walkPositionX = True;
        else:
            #get the amount of X axis pixels the assistant has to walk across the screen
            walkPathX = self.currentX - x;
            #false for walking towards left on the x axis
            walkPositionX = False;
        if y > self.currentY:
            #get the amount of Y axis pixels the assistant has to walk across the screen
            walkPathY = y - self.currentY;
            #true for walking downward on the y axis
            walkPositionY = True;
        else:
            #get the amount of Y axis pixels the assistant has to walk across the screen
            walkPathY = self.currentY - y;
            #false for walking upward on the y axis
            walkPositionY = False;
        #returns the list containing the amount of x and y pixels that need to be traversed across the screen. as wel as boolean values that represent either left/right(x axis) or up/town(y axis) so that the assistant will know which side of the screen to walk towards.
        return [[walkPathX,walkPositionX],[walkPathY,walkPositionY]];

    #method that checks if the character is still walking and in which direction it should walk and therefor which animation movement set needs to be chosen in term of gif images. @takes bool,string @returns bool or void
    def stillWalking(self,resetAnimationSet,direction):
        if resetAnimationSet == False:
            #grab the gif images in order for the specified animation set
            self.chooseAnimationSet("ghost",direction);
            return True;
    
    #method that moves the character across the screen.This method is run inside of a thread. @takes tuple @returns None
    def getAllPathPixels(self,targetPosition):
        self.spriteMoving = True;
        #checks if the animationset needs to be reset and to what. Since the character can walk in multiple random directions.
        resetAnimationSet = False;
        x = targetPosition[0];
        y = targetPosition[1];
        #data containing the distance in x and y coordinates that the assistant has to walk towards its target as well as which ways to walk(up,down,left,right) representeded as boolean values.
        xData, yData = self.getDifference(x,y);
        #grab the amount of x and y pixels the assistant has to walk across the screen to reach its target
        stepsX, stepsY = xData[0], yData[0];
        #as long as the assistant has not reaches the target x,y coordinates keep running this loop
        while stepsX > 0 and stepsY > 0:
            if xData[1] == True and yData[1] == True:
                #choose the walk right animation set to walking rightward
                resetAnimationSet = self.stillWalking(resetAnimationSet,"right");
                #if there are x axis steps left
                if stepsX > 0:
                    #increment the current position of x by 1 simulating rightward movement
                    self.currentX += 1;
                    #decrement the amount of steps needed to be taken on the x axis by 1
                    stepsX -= 1;
                #if there are y axis steps left
                if stepsY > 0:
                    #increment the current position of y by 1 simulating downward movement
                    self.currentY += 1;
                    #decrement the amount of steps needed to be taken on the y axis by 1
                    stepsY -= 1;
                    
            elif xData[1] == False and yData[1] == False:
                #
                resetAnimationSet = self.stillWalking(resetAnimationSet,"left");
                #if there are x axis steps left
                if stepsX > 0:
                    #decrement the current position of x by 1 simulating leftward movement
                    self.currentX -= 1;
                    #decrement the amount of steps needed to be taken on the x axis by 1
                    stepsX -= 1;
                #if there are y axis steps left
                if stepsY > 0:
                    #decrement the current position of y by 1 simulating upward movement
                    self.currentY -= 1;
                    #decrement the amount of steps needed to be taken on the y axis by 1
                    stepsY -= 1;
            
            elif xData[1] == True and yData[1] == False:
                ##choose the walk right animation set to walking rightward
                resetAnimationSet = self.stillWalking(resetAnimationSet,"right");
                #if there are x axis steps left
                if stepsX > 0:
                    #increment the current position of x by 1 simulating rightward movement
                    self.currentX += 1;
                    #decrement the amount of steps needed to be taken on the x axis by 1
                    stepsX -= 1;
                #if there are y axis steps left
                if stepsY > 0:
                    #decrement the current position of y by 1 simulating upward movement
                    self.currentY -= 1;
                    #decrement the amount of steps needed to be taken on the y axis by 1
                    stepsY -= 1;
            
            elif xData[1] == False and yData[1] == True:
                #
                resetAnimationSet = self.stillWalking(resetAnimationSet,"left");
                #if there are x axis steps left
                if stepsX > 0:
                    #decrement the current position of x by 1 simulating leftward movement
                    self.currentX -= 1;
                    #decrement the amount of steps needed to be taken on the x axis by 1
                    stepsX -= 1;
                #if there are y axis steps left
                if stepsY > 0:
                    #increment the current position of y by 1 simulating downward movement
                    self.currentY += 1;
                    #decrement the amount of steps needed to be taken on the y axis by 1
                    stepsY -= 1;
            #wait a very short time for animation purposes. This way there is a tiny delay between the window's(assistant) movement for each single pixel it moves.
            time.sleep(0.001);
            #change the mainwindow geometry to the new self.currentX and self.currentY coordinates of which at least one have been changed by 1 pixel simulating movement.
            self.mainWindow.geometry("+%d+%d" % (self.currentX, self.currentY));
            
        
        
        


class MainWindow(AsyncController,ScreenManager):
    def __init__(self,screenWidth,screenHeight,bgColorToTransparent):
        super().__init__(bgColorToTransparent);
        self.screenWidth = screenWidth;
        self.screenHeight = screenHeight;
        self.mainWindow = tkinter.Tk();
        #set wm attributes to set the specified color to be converted to transparent. this is so that transparent backgrounds can be created for our animated character.
        self.mainWindow.wm_attributes("-transparentcolor", bgColorToTransparent)
        self.mainWindow.resizable(False,False);
        self.currentX = self.mainWindow.winfo_x();
        self.currentY = self.mainWindow.winfo_y();

        #self.bgColorToTransparent = "khaki4";

    def closeWindow(self,icon,item):
        icon.stop();
        self.mainWindow.destroy();

    def showWindow(self,icon,item):
    
        icon.stop();
        self.mainWindow.after(0,self.mainWindow.deiconify());

    def hideWindow(self):
        self.mainWindow.withdraw();
        image = Image.open("favicon.ico");
        menu = (item("Quit",self.closeWindow), item("Show",self.showWindow));
        icon=pystray.Icon("name",image,"My system tray icon",menu);
        icon.run();

    #method that updates the windw position should be run in a seperate thread @void
    def updateWindowPosition(self):
        #initialize the starting x and y coordinates for the mainwindow.
        self.setCurrentCoords();
        while True:   
            try:
                #choose a random window target position within bounds of the screen
                screenPixels = self.chooseWindowPosition();
                #change the window position with animation between the two positions
                self.getAllPathPixels(screenPixels);
                #update the main window
                self.mainWindow.update();
            #print("Updated Window!");
            except Exception as e:
                print(e);
            self.spriteMoving = False;
            self.chooseAnimationSet("ghost","idle");
            time.sleep(2);
        
        

    def launchMainWindow(self):
        #self.hideWindow();
        self.mainWindow.protocol("WM_DELETE_WINDOW",self.hideWindow);
        #choose the idle animation , default arguments have it set to idle
        self.chooseAnimationSet();
        #start the animation seperate from the main thread
        self.mainWindow.after(0,self.Animate,0);
        #launch the main tkinter window
        self.mainWindow.mainloop();
        
        
        
        

a = GeometryManager();
screensize = a.getScreenSize();

b = MainWindow(screensize[0],screensize[1],"#3A3B3E");
#w = b.simulateScreenPixels();
#sim = b.simulateScreenPixels();
b.loadImage("idleGhost.gif",0);
b.createImageBackground(b.getImageGeometry());
#b.animate("idleGhost.gif",10);
b.create_tasks([b.updateWindowPosition]);
#b.create_tasks([b.animate],args=("idleGhost.gif",10));

b.launchMainWindow();

